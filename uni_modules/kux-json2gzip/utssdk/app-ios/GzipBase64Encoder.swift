import Foundation

public class GzipBase64Encoder {
    // 压缩字符串为gzip并转换为base64
    func compressAndEncode(_ string: String) -> String? {
        guard let data = string.data(using: .utf8) else { return nil }
        return compressData(data)?.base64EncodedString()
    }
    
    // 压缩Data为gzip
    private func compressData(_ data: Data) -> Data? {
        return data.gzipped()
    }
}

// 扩展Data，添加gzip压缩方法
extension Data {
    func gzipped() -> Data? {
        var stream = gzip_stream()
        guard deflateInit2(&stream, Z_DEFAULT_COMPRESSION, Z_DEFLATED, MAX_WBITS + 16, 8, Z_DEFAULT_STRATEGY) == Z_OK else {
            return nil
        }
        
        var outputData = Data()
        stream.next_in = self.withUnsafeBytes { (bytes: UnsafeRawBufferPointer) in
            return bytes.bindMemory(to: UInt8.self).baseAddress
        }
        stream.avail_in = uInt(self.count)
        
        let bufferSize = 1024
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: bufferSize)
        stream.next_out = buffer
        stream.avail_out = uInt(bufferSize)
        
        while stream.avail_in > 0 {
            let result = deflate(&stream, Z_SYNC_FLUSH)
            if result != Z_OK && result != Z_STREAM_END {
                buffer.deallocate()
                return nil
            }
            
            let outputSize = bufferSize - Int(stream.avail_out)
            if outputSize > 0 {
                outputData.append(buffer, count: outputSize)
            }
            
            stream.next_out = buffer
            stream.avail_out = uInt(bufferSize)
        }
        
        deflateEnd(&stream)
        buffer.deallocate()
        
        return outputData
    }
}