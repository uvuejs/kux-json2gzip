import { defineConfig } from "vite";
import uni from "@dcloudio/vite-plugin-uni";
import fs from 'fs';
import path from 'path';
import type { Loader } from 'esbuild'

function removeJSComments(code) {
  const singleLineCommentRegex = /\/\/.*(?=\n)/g;
  const multiLineCommentRegex = /\/\*[\s\S]*?\*\//g;

  code = code.replace(multiLineCommentRegex, '');
  code = code.replace(singleLineCommentRegex, '');

  return code;
}

export default defineConfig({
  plugins: [
    uni()
  ],
  optimizeDeps: {
    esbuildOptions: {
      plugins: [
        {
          name: "pako:dep-scan",
          setup(build) {
            build.onLoad({ filter: /\.(?:j|t)sx?$|\.mjs$/ }, ({ path: id }) => {
              let ext = path.extname(id).slice(1)
              if (ext === 'mjs') ext = 'js'
              if (fs.existsSync(id) && id.indexOf('node_modules/pako') > -1) {
                let contents = fs.readFileSync(id, 'utf-8')
                if (contents.includes('#endif')) {
                  contents = removeJSComments(contents)
                }
                return {
                  loader: ext as Loader,
                  contents,
                }
              }
            })
          },
        }
      ]
    }
  }
});
